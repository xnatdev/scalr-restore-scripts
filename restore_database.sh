#!/usr/bin/bash

sudo su - xnat -c 'cd /data/xnat; dropdb xnat; createdb xnat; psql xnat < xnat_database.sql; echo "XNAT database restored last: $(date)." > xnat_database_restore.log'

